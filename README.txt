The two files provided here summarize the count of samples across studies in version 2 of GRASP by Phenotype and PhenotypeCategory. Specifically, the TotalSamples(discovery+replication) column was used.

GRASPv2_samples_by_pheno.txt.gz          - summarized by Phenotype
GRASPv2_samples_by_phenocat.txt          - summarized by PhenotypeCategory

Note, the sum of samples across studies should be considered a rough approximation as it is very common for later studies to re-use existing samples from earlier studies. No attempt was made by the GRASP authors or OmicSoft to determine a count of unique subjects across studies.

GRASP Terms of Use from https://grasp.nhlbi.nih.gov/Terms.aspx
By accessing and using this catalog you agree to the following terms:

You will not repost the full catalog or significant subsets elsewhere, or use for commercial purposes, without permission. Contact Andrew D. Johnson at NHLBI for permissions and/or subscribe to the GRASP-GWAS-L listserv and send a request.

You will not use this information or any subset of this information in order to develop or apply methods aimed at the breach of individual study participant's privacy and confidentiality. For a discussion of related matters see Johnson et al., PLoS Gen 2011 and related publications.

If this information or any subset is used in a scientific publication you agree to cite this resource as well as the underlying GWAS paper(s) which directly contribute to the subsequent publication. Pubmed identifiers are given for sources of each individual resource in order to facilitate appropriate citation. This resource can be cited as "NHLBI GRASP catalog, Build X" where X is the current Build number.

If you use the catalog in a manuscript please cite: Leslie R, O�Donnell CJ, Johnson AD (2014) GRASP: analysis of genotype-phenotype results from 1,390 genome-wide association studies and corresponding open access database. Bioinformatics, 30(12), i185-94. GRASP Build 2.0.0.0


References:

Leslie R, O�Donnell CJ, Johnson AD (2014) GRASP: analysis of genotype-phenotype results from 1,390 genome-wide association studies and corresponding open access database. Bioinformatics, 30(12), i185-94. GRASP Build 2.0.0.0