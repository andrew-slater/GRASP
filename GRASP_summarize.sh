#!/bin/bash
#Andrew Slater
#Summarizing phenotypes with sample counts per request from Yair @ CompuGen
#August 31, 2016

wget https://s3.amazonaws.com/NHLBI_Public/GRASP/GraspFullDataset2.zip

#Confirming for a given PMID and Phenotype, same TotalSamples(discovery+replication) across all variants (records)
zcat GraspFullDataset2.zip | cut -f 8,12 | sort -u | wc -l #182029
zcat GraspFullDataset2.zip | cut -f 8,12,25 | sort -u | wc -l #182030

#Looking for inconsistent PMID Phenotype combination
zcat GraspFullDataset2.zip | cut -f 8,12,25 | sort -u | cut -f 1,2 | uniq -d
#21302346        Biploar disorder (bipolar affective disorder)
#Manual review indicate some records where replication count reflects samples indpendent of discovery and others where replication count is sum of discovery and indepdenent samples. In latter case, discovery are double counted in the TotalSamples(discovery+replication) column so excluding these


zcat GraspFullDataset2.zip | cut -f 8,12,25 | tail -n +2 | sort -u | sort -t $'\t' -k 2,2 | grep -v $'^21302346\tBiploar disorder (bipolar affective disorder)\t2943$' |  awk 'BEGIN {FS="\t"; OFS="\t"; pheno=""; sum=0; print "Phenotype", "TotalSamples(discovery+replication)";}{if (pheno==$2) {sum = sum + $3;} else {print pheno, sum; sum=$3; pheno=$2;}} END{print pheno, sum;}' | grep -v $'^\t0$'  | gzip >GRASPv2_samples_by_pheno.txt.gz

#Confirming for a given PMID, same PaperPhenotypeCategories and TotalSamples(discovery+replication)
zcat GraspFullDataset2.zip | cut -f 8 | sort -u | wc -l #2045
zcat GraspFullDataset2.zip | cut -f 8,14,25 | sort -u |  grep -v $'^21302346\tNeuro;Behavioral;Bipolar disorder\t2943$' | wc -l #2045


zcat GraspFullDataset2.zip | cut -f 8,14,25 | tail -n +2 | sort -u | grep -v $'^21302346\tNeuro;Behavioral;Bipolar disorder\t2943$' |  awk 'BEGIN {FS="\t"; OFS="\t";}{split($2,phenocat,";"); for (i in phenocat) {print phenocat[i], $3;}}' | sort -t $'\t' -k 1,1 | awk 'BEGIN {FS="\t"; OFS="\t"; phenocat=""; sum=0; print "PhenotypeCategory", "TotalSamples(discovery+replication)"}{if (phenocat==$1) {sum = sum + $2;} else {print phenocat, sum; sum=$2; phenocat=$1;}} END{print phenocat, sum;}' | grep -v $'^\t0$'  >GRASPv2_samples_by_phenocat.txt

zip GRASPsummary.zip README.txt GRASPv2_samples_by_phenocat.txt GRASPv2_samples_by_pheno.txt.gz